﻿using UnityEngine;
using System.Collections;

public class StableFPSCounter : MonoBehaviour {

	public bool on;

	private int qty = 0;
	private float currentAvgFPS = 0;
	private float currFps = 0;

	private float worstFps = 0;
	private float worstTimer = 0;
	private float shownWorst = 0;

	void Update () {
		currFps = (1/Time.deltaTime) * Time.timeScale;
		currentAvgFPS = UpdateCumulativeMovingAverageFPS(currFps);

		if(currFps < worstFps) {
			worstFps = currFps;
		}
		worstTimer += Time.deltaTime;
		if(worstTimer > 0) {
			worstTimer -= 1f;
			shownWorst = worstFps;
			worstFps = 10000;
		}

		if(Input.touchCount > 3 && Input.touches[3].phase == TouchPhase.Began)on = !on;
	}

	float UpdateCumulativeMovingAverageFPS(float newFPS)
	{
		qty++;
		qty = Mathf.Clamp(qty,0,15);
		currentAvgFPS += (newFPS - currentAvgFPS)/qty;
		
		return currentAvgFPS;
	}

	void OnGUI () {
		if(!on) return;
		GUI.Label(new Rect(Screen.width-50,0f,70,70),Mathf.Round(currFps)+" fps\n"+Mathf.Round(currentAvgFPS)+" avg\n"+Mathf.Round(shownWorst)+" worst");
	}

	void Start() {
		if(PlayerPrefs.GetInt("DEBUGMODE") != 1)on = false;
	}
}
