﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public static class MaxinRandomUtils {

	//Helps in getting rid of unnecessary children easily, by destroying them instantly, so that they may be replaced with better children
	public static void DestroyChildren(Transform parent) {
		while(parent.childCount > 0) {
			GameObject.DestroyImmediate(parent.GetChild(0).gameObject);
		}
	}

	public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits = false) {
		string suffix = "";
		long lenght;
		if (useBits) lenght = byteLenght * 8;
		else lenght = byteLenght;
		
		if (lenght < 1024) {
			if (useBits) suffix = " bits";
			else suffix = " B";
			return lenght + suffix;
		}
		else if (lenght < 1048576) {
			if (useBits) suffix = " kbits";
			else suffix = " kB";
			return (lenght / 1024) + suffix;
		}
		else {
			if (useBits) suffix = " Mbits";
			else suffix = " MB";
			return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
		}
	}

	public static byte[] ReadStreamFully(Stream input)
	{
		input.Position = 0;
		byte[] buffer = new byte[16*1024];
		using (MemoryStream ms = new MemoryStream())
		{
			int read;
			while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				ms.Write(buffer, 0, read);
			}
			return ms.ToArray();
		}
	}

	static System.Random randomGen = null;

	public static long GetRandomLong() {
		if(randomGen == null)randomGen = new System.Random();
		return randomGen.NextInt64();
	}

	public static long NextInt64(this System.Random rnd)
	{
		var buffer = new byte[sizeof(long)];
		rnd.NextBytes(buffer);
		return System.BitConverter.ToInt64(buffer, 0);
	}

	public static StringBuilder DebugPrintDirStructure (string temporaryCachePath)
	{
		StringBuilder sb = new StringBuilder("Dumping folder structure below "+temporaryCachePath);

		AddItemsToSB(new DirectoryInfo(temporaryCachePath),sb);

		Debug.Log(sb.ToString());

		return sb;
	}

	static void AddItemsToSB (DirectoryInfo directoryInfo,StringBuilder sb)
	{
		foreach (var item in directoryInfo.GetFileSystemInfos()) {
			sb.Append(item.FullName);

			try {
				if((item.Attributes & FileAttributes.Directory) == FileAttributes.Directory) {
					AddItemsToSB((item as DirectoryInfo),sb);
				}
			}
			catch {
				sb.Append("(blocked)");
			}
			sb.AppendLine();
		}
	}

	public static int BoolToInt (bool b)
	{
		if(b)return 1;
		else return 0;
	}
	public static bool IntToBool(int i) {
		return i == 1;
	}
}
