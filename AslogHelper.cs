﻿using UnityEngine;
using System.Collections;

public class AslogHelper : MonoBehaviour {

	public bool loggingOn = true;
	public bool logFast = false;

	// Use this for initialization
	void Start () {
		if (!Application.isEditor) {
			DestroyImmediate(this);
			return;
		}

		Aslog.mainThread = System.Threading.Thread.CurrentThread;
		if(!Application.isEditor)Aslog.fast = false;

		//logFast = Aslog.fast;
		//loggingOn = Aslog.doLog;
	}
	
	void Update() {		
		if(Input.touchCount > 3 && Input.touches[3].phase == TouchPhase.Began) {
			logFast = !Aslog.fast;
#if !UNITY_STANDALONE
			Handheld.Vibrate();
#endif
		}
		if(Input.touchCount > 2 && Input.touches[2].phase == TouchPhase.Began) {
			loggingOn = !Aslog.doLog;
#if !UNITY_STANDALONE
			Handheld.Vibrate();
#endif
			//SoundManager.instance.PlayEffect(SoundEffectType.AddReps,1f,loggingOn ? 2f : 0.5f);
		}

		if(logFast != Aslog.fast)Aslog.fast = logFast;
		if(loggingOn != Aslog.doLog)Aslog.doLog = loggingOn;
	}
}
