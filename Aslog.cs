﻿//COMMENTED BECAUSE COMPILED BINARY (DLL) OF IS IN USE INSTEAD SO THAT CLICKING THE CONSOLE WORKS


#define RIBALLUS
#if RIBALLUS
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.Text;

using System.Linq.Expressions;

public class Aslog {
	
	public static bool doLog {
		get {return m_doLog;}
		set {
			m_doLog = value;
			Log("Logging on/off set to:"+value,ignoreOff:true);
		}
	}
	private static bool m_doLog = true;
	
	public static bool fast {
		get {return m_fast;}
		set {
			m_fast = value;
			Log("Fast logging hack set to:"+value,ignoreOff:true);
		}
	}
	private static bool m_fast = false;
	
	
	public static bool printClassName = true;
	public static bool hideStupidLine = true;
	public static bool openArrays = true;

	public static bool doRandomColoring = true;

	private static string[] colorList = {
		"black",
		"blue",
		"brown",
		"darkblue",
		"green",
		"grey",
		/*"lightblue",*/
		/*"lime",*/
		"maroon",
		"navy",
		/*"olive",*/
		/*"orange",*/
		"purple",
		/*"red",*/
		/*"silver",*/
		"teal",
		/*"white",*/
		"yellow"
	};
	
	public static System.Threading.Thread mainThread; //Needs to be set from the main thread to avoid exceptions when fast logging gets called from a secondary thread
	
	public static void Log(object toLog, int ignoreStackDepth = 1, bool ignoreOff = false, bool overrideEnableFast = false) {
		if(!doLog && !ignoreOff)return;

#if UNITY_IOS
		if(toLog == null) {
			UnityEngine.Debug.Log("Aslog:tried to log null on ios");
			return;
		}
		
		UnityEngine.Debug.Log(toLog.ToString());
		return;
#endif
		
		string msg;
		
		//UnityEngine.Debug.LogWarning(toLog is ICollection);
		
		if(openArrays && toLog is ICollection) {
			StringBuilder sb = new StringBuilder("ARRAY, length:"+(toLog as ICollection).Count+"\n");
			foreach(var item in toLog as ICollection) {
				sb.AppendLine(item.ToString());
			}
			msg = sb.ToString();
		}
		else if(toLog != null)msg = toLog.ToString();
		else msg = "NULL";
		
		//object callingObject;
		if(printClassName) {
			StackTrace trace = new StackTrace();
			
			StackFrame frame = trace.GetFrame(ignoreStackDepth);
			
			string className = frame.GetMethod().DeclaringType.Name;
			string methodName = frame.GetMethod().Name;
			
			
			//msg = string.Format("[{0}.{1}]#### {2}",className,methodName,msg);
			//msg = string.Format("{2}\n<b>[{0}.{1}]</b>",className,methodName,msg);
			msg = string.Format("<b>{2}</b>\n[{0}.{1}]",className,methodName,msg);

			if(doRandomColoring) {
				msg = string.Format("<color={0}>{1}</color>{0}",GetClassColorString(frame.GetMethod().DeclaringType),msg);
			}
		}
		
		if(hideStupidLine)msg+="\n";

		if(overrideEnableFast)fast = true;

		bool isEditor = false;

#if UNITY_EDITOR
		isEditor = true;
#endif
		if(isEditor && mainThread != null) {
			bool isMainThread = (System.Threading.Thread.CurrentThread == mainThread);
			if(fast && isMainThread) {
				typeof (UnityEngine.Debug).GetMethod (
					"LogPlayerBuildError",
					BindingFlags.Static | BindingFlags.InvokeMethod | BindingFlags.NonPublic
					).Invoke (
					null,
					new object[]{msg, "", 0, 0}
				);
			}
			else UnityEngine.Debug.Log(msg);
		}
		else UnityEngine.Debug.Log(msg);
	}
	
	public static void LogFormat (string format, params object[] parts)	{
		Log(string.Format(format,parts),2);
	}

	public static void LogMany(params object[] objs) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < objs.Length; i++) {
			sb.Append(objs[i].ToString());
		}
		Log(sb.ToString(),ignoreStackDepth:2);
	}

	public static string GetClassColorString(System.Type type) {

		//UnityEngine.Random.seed can only be set from main thread lol what
		//Random.seed = (int)type.Name[0];
		//return colorList[Random.Range(0,colorList.Length)];

		var randomGen = new System.Random((int)type.Name[0]);
		return colorList[randomGen.Next(colorList.Length-1)];
	}

	private static Stopwatch timer;
	private static string timerTag;

	public static void LogTimerStart(string tag) {
		timer = Stopwatch.StartNew();
		timerTag = tag;
	}
	public static void LogTimerStop() {
		Log(string.Format("Timer {0} completed: {1} ms",timerTag,timer.ElapsedMilliseconds));
	}

	//Interval timer
	static Stopwatch intervalTimer;
	static long intervalTimerLastInterval;
	static string intervalTimerName;
	static List<string> intervalTimerTags;
	//List<long> intervalTimerIntervals;

	public static void StartIntervalTimer(string inIntervalTimerName) {
		if(intervalTimer != null)UnityEngine.Debug.LogError("TRIED TO START INTERVAL TIMER WHILE ALREADY IN PROGRESS");

		intervalTimerName = inIntervalTimerName;
		Log("Starting interval timer "+intervalTimerName);
		intervalTimerLastInterval = 0;
		intervalTimerTags = new List<string>(50);
		//intervalTimerIntervals = new List<long>(50);
		intervalTimer = Stopwatch.StartNew();
	}

	public static void TimerInterval (string tag) {
		if(intervalTimer == null)return;
		long totalMSNow = intervalTimer.ElapsedMilliseconds;
		long intervalMS = intervalTimer.ElapsedMilliseconds - intervalTimerLastInterval;

		string toLog = string.Format("Interval:{0}ms, total: {1}ms ({2})",WithLeadingSpaces(intervalMS),WithLeadingSpaces(totalMSNow),tag); //TODO get out of here's (should only have very minimal processing)
		/*if(timerInstant);//Console.WriteLine(toLog);
		else {*/
			intervalTimerTags.Add(toLog);
		//}
		
		intervalTimerLastInterval = totalMSNow;
	}
	static string WithLeadingSpaces (long num)
	{
		return string.Format("{0,4:###0}",num);
	}

	public static void PauseIntervalTimer() {
		intervalTimer.Stop();
	}
	public static void ResumeIntervalTimer() {
		intervalTimer.Start();
	}

	public delegate void IntervalTimerCompletedDelegate(string msg,bool onMainThread);
	public static IntervalTimerCompletedDelegate onIntervalTimerCompleted;

	public static void StopIntervalTimer () {
		if(intervalTimer == null)return;
		StringBuilder sb = new StringBuilder();
		sb.AppendLine(string.Format("Interval timer "+intervalTimerName+" stopped, total:{0}ms",intervalTimer.ElapsedMilliseconds));
		if(intervalTimer.ElapsedMilliseconds > 10000)sb.AppendLine("(Converted to readable: "+new System.TimeSpan(0,0,0,0,(int)intervalTimer.ElapsedMilliseconds)+")");
		
		//if(!timerInstant) {
			//Log cached entries			
			sb.AppendLine("Timer intervals:");
			for (int i = 0; i < intervalTimerTags.Count; i++) {
				sb.AppendLine(intervalTimerTags[i]);
				//Log (intervalTimerTags[i]); //temporary
			}
		//}
		string str = sb.ToString();
		Log (str,ignoreOff:true);

		bool onMainThread = (mainThread != null && System.Threading.Thread.CurrentThread == mainThread);
		//if() {
			if(onIntervalTimerCompleted != null)onIntervalTimerCompleted(intervalTimerName+"|||"+str,onMainThread);
		//}

		intervalTimer.Stop();
		intervalTimer = null;
	}


	/*public static void LogObjectsVerbose(string firstPart,params object[] objectsToExplain) {
		StringBuilder sb = new StringBuilder(firstPart);

		for (int i = 0; i < objectsToExplain.Length; i++) {
			//sb.Append(Check(() => objectsToExplain[i]));
			sb.Append
			sb.Append(":"+objectsToExplain[i]);
			//sb.AppendLine();
		}
		Log(sb.ToString());
	}*/

	/*static string GetName<T>(T item) where T : class 
	{
		return typeof(T).GetProperties()[0].Name;
	}*/

	/*static string Check<T>(Expression<System.Func<T>> expr) {
		var body = ((MemberExpression)expr.Body);
		return body.Member.Name;
	}*/
	
}
#endif